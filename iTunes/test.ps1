$iTunesApp = New-Object -Com iTunes.Application
$tracks = $iTunesApp.LibraryPlaylist.Tracks
$results2015 = @{}
$resultsOlder = @{}

for ( $i=1 ; $i -le $tracks.Count ; $i++ ) {
    $song = $tracks.Item($i)
    if ( $song.Year -eq 2015 ) {
		if ( $song.Compilation -eq $true ) {
			$results2015["Various Artists - " + $song.Album] += $song.PlayedCount
		} else {
			$results2015[$song.Artist + " - " + $song.Album] += $song.PlayedCount
		}
    } elseif ( $song.Year -lt 2015 -and $song.DateAdded.Year -eq 2015 ) {
		if ( $song.Compilation -eq $true ) {
			$resultsOlder["Various Artists - " + $song.Album] += $song.PlayedCount
		} else {
			$resultsOlder[$song.Artist + " - " + $song.Album] += $song.PlayedCount
		}
    }
}

Write-Host "2015 Releases"
$results2015.GetEnumerator() | 
	Sort-Object Value -Descending | 
	Select -First 30 |
	Format-Table -AutoSize

Write-Host "Older releases, added in 2015"
$resultsOlder.GetEnumerator() | 
	Sort-Object Value -Descending | 
	Select -First 30 |
	Format-Table -AutoSize