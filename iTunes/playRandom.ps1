$iTunesApp = New-Object -Com iTunes.Application
$tracks = $iTunesApp.LibraryPlaylist.Tracks

$numHigh = $tracks.Count
$numLow = 1
$numRandom = New-Object System.Random

$song = $tracks.Item($numRandom.Next($numLow, $numHigh))
$song.Play()
# Write-Output "What is the name of this song?"
# Start-Sleep -s 10
# $iTunesApp.Stop()
# Write-Output "Answer: $($song.Artist) - $($song.Name)"

Write-Output "Currently playing: $($song.Artist) - $($song.Name)"