﻿$iTunesApp = New-Object -Com iTunes.Application
$tracks = $iTunesApp.LibraryPlaylist.Tracks

$albumCount = @{}


foreach ($track in $tracks) {
    if ( $null -eq $track.AlbumArtist ) {
        $album = 'Various Artists - ' + $track.Album
    } else {
        $album = $track.AlbumArtist + ' - ' + $track.Album
    }

    #$album

    if ( $albumCount.ContainsKey($album) ) {
        $albumCount[$album] += $track.PlayedCount
    } else {
        $albumCount.Add($album, $track.PlayedCount)
    }

}

$albumCount.GetEnumerator() | Sort-Object -Property Value -Desc | Select-Object -First 50 | Format-Table -AutoSize