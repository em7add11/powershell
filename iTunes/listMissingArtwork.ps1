$iTunesApp = New-Object -Com iTunes.Application
$tracks = $iTunesApp.LibraryPlaylist.Tracks

for ( $i=1 ; $i -le $tracks.Count ; $i++ ) { 
    $song = $tracks.Item($i)
    if ( $song.Artwork.Count -eq 0 -and -not $song.Podcast ) { 
		$trackData = New-Object PSObject -Property @{
			Artist = $song.Artist
			Album  = $song.Album
			Track  = $song.Name
		}
		Write-Output $trackData
    }
}