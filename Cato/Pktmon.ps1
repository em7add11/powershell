# Author: John Farthing
# Version: 20220127

<#
.SYNOPSIS
	A PowerShell wrapper for the pktmon packet capture tool to generate a client PCAP for troubleshooting VPN client issues.

.DESCRIPTION
	This script will confirm the VPN client adapater exists and then prompt for whether to capture on this specific adapater or all NICs. While script is running, generate any network traffic to be examined by Cato support. Once the script has completed it will output a PCAPNG file to provide to Cato support for analysis.

	It requires the ability to run unsigned powershell scripts on the client computer. This can be checked via Get-ExecutionPolicy.

.INPUTS
	None. You cannot pipe objects to Pktmon.ps1.

.OUTPUTS
	Outputs a PCAPNG file to desktop of current user.

.PARAMETER timer
	A value between 0 and 300 seconds. If not specified or set to 0, packet capture will run until stopped by user.

.EXAMPLE
	PS> .\pktmon.ps1
#>

param(
	[ValidateRange(0,300)]
	[Parameter(Mandatory = $false)]
	[int]$Timer = 0
)

$graceful = $false

$workingDir = [Environment]::GetFolderPath("Desktop")
$tempFile = Join-Path $workingDir 'temp.etl'
$pcapFile  = Join-Path $workingDir ('pktmon-' + (get-date -Format yyyyMMddTHHmmss) + '.pcapng')

# Check if prompt is elevated
$currentPrincipal = New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent())
if ( -not $currentPrincipal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator) ) {
	Write-Host "Pktmon requires elevated prompt`n"
	exit 0
}

# Confirm Cato VPN adapter exists
try {
    $intIndex = [int](pktmon comp list | Where-Object { $_ -match 'Cato' }).Trim().Split(' ')[0]
}
catch {
    Write-Host "Cato VPN adapter not found, exiting.`n"
    exit 1
}

# Let user pick if capturing on all NICs or just VPN adapter
$msgTitle = ""
$msgPrompt = "Select network interface to capture on:"
$choices = [System.Management.Automation.Host.ChoiceDescription[]] @("&All NICs", "&Cato VPN Adapter")
$default = 1
$choice = $host.UI.PromptForChoice($msgTitle, $msgPrompt, $choices, $default)

switch ($choice) {
    0 { $interface = 'nics'} # All NICs
    1 { $interface = $intIndex } # Cato VPN only
}

# Create pktmon filter and start capture
try {
	pktmon filter add ipv4 -d ipv4
	pktmon filter add ipv6 -d ipv6
	pktmon filter add arp -d arp

	pktmon start -c --comp $interface --pkt-size 0 -f $tempFile

	Write-Host "`nPacket capture running"

	if ( $timer -gt 0) {
		Start-Sleep -Seconds $timer
	} else {
		Read-Host "`nPress Enter to stop (do not use ctrl-c)"
	}
	$graceful = $true
}

# Make sure pktmon is stopped even if ctrl-c is hit (buggy cause of read-host)
# Only create PCAP and remove ETL if stopped normally
finally {
	pktmon stop
	pktmon filter remove
	
	if ( $graceful ) {
		$output = pktmon etl2pcap $tempFile -o $pcapFile 2>&1

        if ( $output -match "Error" ) {
            Write-Host $output[0]
        }
        else {
    		Write-Host "`nPCAP location:" $pcapFile
            Remove-Item -force $tempFile
        }
	}
}