param(
    [string]$key = $null,
    [int]$id = $null,
    [switch]$help

)

# Send query to Cato API, returns PSCustomObject
function SendQuery {
  param (
    $QueryData
  )

  $res = $null
  $retryCount = 0

  While ( $retryCount -lt 10 ) {
    Try {
      $res = Invoke-RestMethod @QueryData
	  break
    }
    Catch {
      Write-Debug "ERROR $retrycount - $($error[0].Exception.Message)"
      $retryCount++
      Start-Sleep -Seconds 2
    }
  }

  If ($null -eq $res) {
    return $false, $error[0].Exception.Message
  }
  else {
    return $true, $res
  }
}

# Show usage help if missing params
if ($apiKey -eq '' -or $accountId -eq '' -or $help) {
    Write-Host "Usage: $($MyInvocation.MyCommand.Name) -key APIKEY -id ID`n"
    Exit
}

###################
### Main Script ###
###################

#$DebugPreference = 'Continue'
$startTime = Get-Date

$query = Get-Content -Raw .\query.txt

# Remove formatting from query string
$body = @{
    query = $query -replace "(\s+|\n)"," "
}

# Assemble all API query parameters
$params = @{
    Method = "POST"
    Uri = 'https://api.catonetworks.com/api/v1/graphql2'
    Headers = @{ "x-api-key" = $key }
    Body = $body | ConvertTo-Json
    ContentType = "application/json"
}

$apiRes = SendQuery $params

# Print output, format if JSON
if ( -not $apiRes[0] ) {
  $apiRes[1]
}
else {
  $apiRes[1] | ConvertTo-Json -Depth 100
}

Write-Debug "Total run time $(((Get-Date) - $startTime).TotalSeconds) seconds" 