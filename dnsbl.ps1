param( [IpAddress] $ip = '127.0.0.1')

$blacklistZones = 'sbl-xbl.spamhaus.org','bl.spamcop.net','dnsbl.sorbs.net'

$ipTmp = $ip.ToString().Split('.')

[array]::Reverse($ipTmp)

$ipPtr = $ipTmp -join '.'

foreach ($blacklistZone in $blacklistZones) {
    $lookupHost = $ipPtr + '.' + $blacklistZone
    Write-Host -ForegroundColor Green "Looking up: $lookupHost`n"
    
    Try {
        $res = Resolve-DnsName -Type A $lookupHost -ErrorAction Stop

        Write-Host -ForegroundColor Red "`tFound on blacklist:" ($res.IP4Address | Sort-Object {$_ -as [version]}) "`n"
    }
    Catch {
        Write-Host "`t" $_.Exception.Message "`n"
    }
}