param (
	#[Parameter(Mandatory)]
	[string]
	$zipDir = (Read-Host -Prompt 'Source zip files'),
	[string]
	$musicDir = (Read-Host -Prompt 'Destination folder')
)

$7zipExe = 'C:\Program Files\7-Zip\7z.exe'

# Prompt for when destination folder already exists for zip
$question = "`tAre you sure you want to proceed?"
$choices = New-Object Collections.ObjectModel.Collection[Management.Automation.Host.ChoiceDescription]
$choices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&Yes'))
$choices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&No'))

If ( (Test-Path $zipDir) ) {
	Write-Host "Scanning $zipDir for zip files...`n"
	$zipfiles = Get-ChildItem -Path $zipDir *.zip #| Select-Object -First 2

	foreach ($zip in $zipfiles) {
		Write-Host -NoNewLine "Found zip file: "
		Write-Host "[$($zip.Name)]" -Foreground DarkGreen

		$proceed = $true
		$outputDir = Join-Path $musicDir $zip.BaseName
		If ( Test-Path $outputDir ) {
			$title = "`tDestination folder already exists: `"$outputDir`""
			$decision = $Host.UI.PromptForChoice($title, $question, $choices, 1)
			If ( $decision -ne 0) {
				Write-Host -NoNewLine "`tFile skipped: "
				Write-Host "$($zip.Name)`n" -Foreground DarkRed
				$proceed = $false
			}
		}

		if ( $proceed ) {
			Write-Host -NoNewLine "`tUnzipping: "
			Write-Host -NoNewLine "[$($zip.Name)]" -Foreground DarkGreen
			Write-Host -NoNewLine " to "
			Write-Host "[$outputDir]`n" -Foreground DarkGreen
			& $7zipExe x -aoa -o"$outputDir" $zip.FullName | Out-Null
		}
	}
} Else {
	Write-Host -NoNewLine "Invalid path for zip files: "
	Write-Host "[$zipDir]`n" -Foreground DarkRed
	exit 1
}